import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.applet.Applet;
import java.net.URL;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Grid extends Applet implements ActionListener {

	private Square[][] gridArray = new Square[0][0];
	private URL base;
	public static Image mine;
	private JButton buttonObj;
	private String buttonText;
	private int width = 20;
	private int height = 20;
	private String difficulty = "easy";
	private ActionEvent arg0;
	
	public void init() {
		TableGenerator tg = new TableGenerator(width, height, difficulty);
		Frame frame = (Frame) this.getParent().getParent();
		frame.setTitle("Minesweeper by Gost :F");
		frame.setMenuBar(null);
		gridArray = tg.generate();
		JButton buttonsArray[][] = new JButton[width][height];
		try {
			base = getDocumentBase();
		} catch (Exception e) {
			// TODO: handle exception
		}

		mine = getImage(base, "mine.jpg");
		setSize(gridArray[0].length * 42, gridArray.length * 42);
		setLayout(new GridLayout(gridArray.length, gridArray[0].length));
		for (Square[] i : gridArray) {
			for (Square ii : i) {
				buttonObj = new JButton("");
				add(buttonObj);
				
				final String buttonText = Integer.toString(ii.getValue());
				final JButton jbtnOne = buttonObj;
				final int squareX = ii.getX();
				final int squareY = ii.getY();
				
				jbtnOne.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jbtnOne.setBackground(Color.white);
						if(!buttonText.equals("-1") && !buttonText.equals("0")){
						jbtnOne.setText(buttonText);
				
						} 	
						
					}
				});
				buttonsArray[squareX][squareY] = buttonObj;
				buttonsArray[squareX][squareY].setName(buttonText);
				
				if (ii.getValue() == -1) {
					final JButton jbtn = buttonObj;
					jbtn.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							jbtn.setBackground(Color.red);
							jbtn.setIcon(new ImageIcon(mine));
							new JOptionPane().showMessageDialog(null, "BOOM");
						}
					});
				} 
			}
		}
//		empty(buttonsArray);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	private void empty(JButton[][] buttonsArray){
		for (int ix = 0; ix < width; ix++) {
			for (int iy = 0; iy < height; iy++) {
				if (buttonsArray[ix][iy].getName().equals("0")) {
					for (int iix = ix - 1; iix <= ix + 1; iix++) {
						for (int iiy = iy - 1; iiy <= iy + 1; iiy++) {
							if (iix >= 0 && iiy >= 0 && iix < width && iiy < height) {
								buttonsArray[iix][iiy].doClick();
								
													}
						}
					}
				}
			}
		}
	}
	
}