import java.awt.List;
import java.util.Random;

public class TableGenerator {

	private Square tableArray[][];
	private int minesPercents;
	private int height;
	private int width;
	private int minesNumber;

	public TableGenerator(int weight, int height, String difficult) {
		tableArray = new Square[weight][height];
		
		switch (difficult) {
		case "easy":
			minesPercents = 8;
			break;
		case "normal":
			minesPercents = 16;
			break;
		case "hard":
			minesPercents = 32;
			break;
		}

		this.width = weight;
		this.height = height;
		minesNumber = weight * height * minesPercents / 100;
		for (int ix = 0; ix < weight; ix++) {
			for (int iy = 0; iy < height; iy++) {
				tableArray[ix][iy] = new Square();
				tableArray[ix][iy].setX(ix);
				tableArray[ix][iy].setY(iy);
			}
		}
	}

	private void createTableMines() {
		Random randomWeight = new Random();
		Random randomHeight = new Random();
		int randWidth;
		int randHeight;

		for (int i = 0; i < minesNumber; i++) {
			randWidth = randomWeight.nextInt(width);
			randHeight = randomHeight.nextInt(height);
//			System.out.println(randWeigh + "  " + randHeigh);
			if (tableArray[randWidth][randHeight].getValue() != -1) {
				tableArray[randWidth][randHeight].setValue(-1);
			} else {
				i -= 1;
			}

		}
	}

	private void nearestMinesNumber() {
		for (int ix = 0; ix < width; ix++) {
			for (int iy = 0; iy < height; iy++) {
				if (tableArray[ix][iy].getValue() == -1) {
					for (int iix = ix - 1; iix <= ix + 1; iix++) {
						for (int iiy = iy - 1; iiy <= iy + 1; iiy++) {
							if (iix >= 0 && iiy >= 0 && iix < width && iiy < height) {
								if (tableArray[iix][iiy].getValue() != -1) {
									tableArray[iix][iiy].setValue(tableArray[iix][iiy].getValue() + 1);
								}
							}
						}
					}
				}
			}
		}
	}

	public Square[][] generate() {
		createTableMines();
		nearestMinesNumber();
		for (int ix = 0; ix < width; ix++) {
			for (int iy = 0; iy < height; iy++) {
//				System.out.print(tableArray[ix][iy] + "\t");
				if (iy == height - 1)
					System.out.println();
			}
		}
		return tableArray;
	}
}
